import { Injectable } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { forkJoin, Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private loc: Location,
    private http: HttpClient, private router: Router, private authService: AuthService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authService.isLogged$()
      .pipe(
        switchMap(isLogged => {
          return isLogged ? this.http.get<{ response: string, error: string}>('http://localhost:3000/validateToken')
            .pipe(
              map(val => val.response === 'ok'),
            ) : of(false)
        }),
        tap(isTokenValid => {
          if (!isTokenValid) {
            this.router.navigateByUrl('login')
          }
        }),
      )

  }
}
