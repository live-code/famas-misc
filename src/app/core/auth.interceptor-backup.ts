import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let clonedReq = req;

    if( this.authService.token$) {
      clonedReq = req.clone({
        setHeaders: {
          Authorization: this.authService.token$
        }
      })
    }
    return next.handle(clonedReq)
      .pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch(err.status) {
              case 0:
              case 401:
              case 404:
              default:
                this.authService.logout()
            }
          }
          return throwError(err)
        })
      )

  }

}
