import { Injectable } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private http: HttpClient, private router: Router, private authService: AuthService, private activatedRouter: ActivatedRoute) {
    console.log(router, activatedRouter)
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.authService.isLogged$()) {
      this.router.navigateByUrl('login')
    }

    return this.http.get<{ response: string, error: string}>('http://localhost:3000/validateToken')
      .pipe(
        map(res => {
          return res.response === 'ok';
        }),
        tap(isValid => {
          if (!isValid) {
            this.router.navigateByUrl('login')
          }
        }),
      )
    // return this.authService.isLogged();
  }
}
