import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Auth } from '../model/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth: Auth | null = null;

  // ------o---o---o-ooo--->

  constructor(private http: HttpClient, private router: Router) {
    const localAuth = localStorage.getItem('auth');
    if (localAuth) {
      this.auth = JSON.parse(localAuth)
    }
  }

  login(username: string, pass: string) {

    const params = new HttpParams()
      .set('user', username)
      .set('pass', pass)

    this.http.get<Auth>('http://localhost:3000/login', { params })
      .subscribe(res => {
        this.auth = res;
        localStorage.setItem('auth', JSON.stringify(this.auth))
        this.router.navigateByUrl('home')
      })
  }

  logout() {
    this.auth = null;
    localStorage.removeItem('auth')
    this.router.navigateByUrl('login')
  }

  isLogged(): boolean {
    return !!this.auth;
  }

  get token(): string | null {
    if (this.auth) {
      return 'Bearer ' + this.auth.token;
    }
    return null;
  }

  checkRole(roles: Role[] ): boolean {
    if (this.auth) {
      return roles.includes(this.auth?.role)
    }
    return false;
  }
}

export type Role = 'admin' | 'moderator' | 'guest';
