import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Auth } from '../model/auth';
import { Router } from '@angular/router';
import { BehaviorSubject, interval, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth$ = new BehaviorSubject<Auth | null>(null)

  constructor(private http: HttpClient, private router: Router) {

    const localAuth = localStorage.getItem('auth');
    if (localAuth) {
      this.auth$.next(JSON.parse(localAuth))
    }
  }

  login(username: string, pass: string) {

    const params = new HttpParams()
      .set('user', username)
      .set('pass', pass)

    this.http.get<Auth>('http://localhost:3000/login', { params })
      .subscribe(res => {
        this.auth$.next(res)

        localStorage.setItem('auth', JSON.stringify(res))
        this.router.navigateByUrl('home')
      })
  }

  logout() {
    this.auth$.next(null);
    localStorage.removeItem('auth')
    this.router.navigateByUrl('login')
  }

  isLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(value => !!value)
      )
  }

  get token$(): Observable<string | null> {
    return this.auth$
      .pipe(
        map(auth => {
          return auth ? 'Bearer ' + auth.token : null
        })
      )
  }

  isRoleValid$(roles: Role[] ): Observable<boolean> {
    return this.auth$
      .pipe(
        map(auth => {
          return auth ?
            roles.includes(auth.role) :
            false
        })
      )
  }
}

export type Role = 'admin' | 'moderator' | 'guest';
