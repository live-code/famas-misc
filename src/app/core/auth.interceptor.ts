import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { combineLatest, forkJoin, Observable, of, Subscription, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { catchError, first, switchMap, withLatestFrom } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) { }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return combineLatest([
      this.authService.isLogged$(),
      this.authService.token$,
    ])
      .pipe(
        first(),
        switchMap(([isLogged, tk]) => {
          return (isLogged && tk) ?
            next.handle(req.clone({ setHeaders: { Authorization: tk } })) :
            next.handle(req)
        }),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch(err.status) {
              case 0:
              case 401:
              case 404:
              default:
                this.authService.logout()
            }
          }
          return throwError(err)
        })
      )
  }

  intercept2(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return this.authService.isLogged$()
      .pipe(
        first(),
        withLatestFrom(this.authService.token$),
        switchMap(([isLogged, tk]) => {
          // const [isLogged, tk] = params;
          return (isLogged && tk) ?
            next.handle(req.clone({ setHeaders: { Authorization: tk } })) :
            next.handle(req)
        }),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch(err.status) {
              case 0:
              case 401:
              case 404:
              default:
                this.authService.logout()
            }
          }
          return throwError(err)
        })
      )
  }

}
