import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'fm-login',
  template: `
    
    <router-outlet></router-outlet>
    <hr>
    <span routerLink="lostpass">lost pass</span> | 
    <span routerLink="registration">registration</span> | 
    <span routerLink="signin">signin</span>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor(private http: HttpClient) {
    http.get('')
  }

  ngOnInit(): void {
  }

}
