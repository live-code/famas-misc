import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../core/auth.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'fm-signin',
  template: `

    <form #f="ngForm" (submit)="loginHandler(f.value)">
      <input type="text" ngModel name="user">
      <input type="text" ngModel name="pass">
      <button type="submit">login</button>
    </form>
  `,
  styles: [
  ]
})
export class SigninComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }


  loginHandler({ user, pass }: { user: string, pass: string}) {
    this.authService.login(user, pass)
  }



  loginHandler3(formData: { user: string, pass: string }) {
    const { user, pass } = formData;
    this.authService.login(user, pass)
  }


  loginHandler2(form: NgForm) {
    /*const user = form.value.user;
    const p = form.value.pass;*/

    // destructing
    const { user, pass: p } = form.value;

    this.authService.login(user, p)
  }

}
