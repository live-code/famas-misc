import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SigninComponent } from './components/signin.component';
import { LostpassComponent } from './components/lostpass.component';
import { RegistrationComponent } from './components/registration.component';



@NgModule({
  declarations: [
    LoginComponent,
    SigninComponent,
    LostpassComponent,
    RegistrationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoginComponent,
        children: [
          { path: 'registration', component: RegistrationComponent},
          { path: 'lostpass', component: LostpassComponent},
          { path: 'signin', component: SigninComponent},
          { path: '', redirectTo: 'signin'}
        ]
      },
    ])
  ]
})
export class LoginModule { }
