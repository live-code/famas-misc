import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';

@Component({
  selector: 'fm-demo-reactiveforms',
  template: `
    
    
    <form [formGroup]="form">
      
      <input type="text" formControlName="name">
      <input type="text" formControlName="surname">
      <div *ngIf="form.get('surname')?.errors?.alphaNumeric">no symbols</div>
      
      <div formGroupName="carInfo" [style.border]="form.get('carInfo')?.invalid ? '3px solid red' : null">
        <input type="text" formControlName="model">
        <input type="text" formControlName="year">
      </div>

      <div *ngIf="form.get('carInfo')?.valid ">
        next info
      </div>

      <div formArrayName="items"
           *ngFor="let item of items.controls; let i = index; let last = last">

        <div [formGroupName]="i" class="form-group form-inline">
          <i class="fa fa-exclamation-circle fa-2x text-danger" *ngIf="item.invalid"></i>
          <i class="fa fa-check-circle fa-2x text-success" *ngIf="item.valid"></i>

          <input formControlName="name" placeholder="Item name *" class="form-control ml-2 mr-2">
          <input formControlName="price" placeholder="Item price *" class="form-control mr-2">

          <!--<i class="fa fa-trash-o fa-2x mr-2" (click)="removeItem(item)" *ngIf="items.controls.length > 1"></i>
          <i class="fa fa-plus-circle fa-2x" (click)="addItem()" *ngIf="item?.valid && last"></i>-->
        </div>
      </div>
        
      <button [disabled]="form.invalid">SUBMIT</button>
      
      
      

      <!--<step2 formControl="carInfo"></step2>-->
    </form>
    
    <pre>{{form.value | json}}</pre>
    
  `,
  styles: [
  ]
})
export class DemoReactiveformsComponent implements OnInit {
  form!: FormGroup;
  items!: FormArray;


  constructor(fb: FormBuilder, http: HttpClient) {
    this.form = fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.compose([Validators.required, Validators.minLength(3), alphaNumeric])],
      carInfo: fb.group({
        model: ['', Validators.required],
        year: ['', Validators.required],
      }),
      items: fb.array([]) as FormArray
    })

    this.items = this.form.get('items') as FormArray;
    this.form.valueChanges
      .pipe(

      )
      .subscribe(console.log)
    /*this.input.valueChanges
      .pipe(
        filter(text => text.length > 3),
        debounceTime(1000),
        distinctUntilChanged(),
        switchMap(text => http.get<any[]>(`https://jsonplaceholder.typicode.com/users?username=${text}`)
      )
      .subscribe(users => {
        console.log(users)
      })
     */

    setTimeout(() => {
      this.form.setValue({
        name: 'a',
        surname: 'b',
        carInfo: {
          model: 'C',
          year: 'D'
        }
      })
    }, 1000)


    setTimeout(() => {
      this.form.patchValue({
        surname: 'bbb',
        carInfo: {
          year: 'DDD'
        }
      })

      this.form.get('carInfo')?.get('model')?.setValue('ciao')

      this.form.addControl('age', new FormControl(''))
    }, 2000)
  }

  ngOnInit(): void {
  }

}

// VALIDATOR
export function alphaNumeric(c: FormControl): ValidationErrors | null {
  const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;
  if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
    return { alphaNumeric: true };
  }
  return null;
}
