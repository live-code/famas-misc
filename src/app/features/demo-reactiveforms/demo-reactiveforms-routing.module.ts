import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoReactiveformsComponent } from './demo-reactiveforms.component';

const routes: Routes = [{ path: '', component: DemoReactiveformsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoReactiveformsRoutingModule { }
