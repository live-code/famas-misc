import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoReactiveformsRoutingModule } from './demo-reactiveforms-routing.module';
import { DemoReactiveformsComponent } from './demo-reactiveforms.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DemoReactiveformsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DemoReactiveformsRoutingModule
  ]
})
export class DemoReactiveformsModule { }
