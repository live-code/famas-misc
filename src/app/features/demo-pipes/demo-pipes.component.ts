import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';

@Component({
  selector: 'fm-demo-pipes',
  template: `

    <select [(ngModel)]="currentGender">
      <option value="">All</option>
      <option value="M">Male</option>
      <option value="F">Female</option>    
    </select>
    <input type="text" [(ngModel)]="currentText">
    <input type="number" [(ngModel)]="currentAge">
    <select [(ngModel)]="sortBy">
      <option value="ASC">ASC</option>
      <option value="DESC">DESC</option>
    </select>
    
    <li *ngFor="let user of (users | 
                              filterByGender: currentGender | 
                              filterByAge: currentAge) |
                              filterByTexxt: currentText |
                              sortBy: sortBy
    ">
      {{user.name}} - {{user.gender}} - {{user.age}}
    </li>
    
    <hr>
    <h2>Date and numbers</h2>
    {{today | date: 'dd MMM yy'}}
    {{today2 | date: 'dd MMMM yyyy @ hh:mm-ss'}}
    {{1231.123999412412 | number: '1.2-4'}}

    <h2>Custom pipe formatGb</h2>
    {{8 | formatGb}}
    {{8 | formatGb: 'kb' : true}}

    <h2>Custom Pipe Mapquest</h2>
    <img [src]="'Trieste' | gmap" width="100%">
    <img [src]="'Trieste' | gmap : 10" width="100%">
    
  `,
  styles: [
  ]
})
export class DemoPipesComponent implements OnInit {
  today = new Date();
  today2 = Date.now();

  currentGender: 'M' | 'F' | ''  = '';
  currentAge: number = 0;
  currentText: string = '';
  sortBy: 'ASC' | 'DESC' = 'ASC';

  users: User[] = [
    { id: 1, name: 'Michele', age: 30, gender: 'M' },
    { id: 2, name: 'Fabio', age: 25, gender: 'F' },
    { id: 3, name: 'Silvia', age: 20, gender: 'F' }
  ];

  ngOnInit(): void {
  }

}
