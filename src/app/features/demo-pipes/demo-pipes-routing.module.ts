import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoPipesComponent } from './demo-pipes.component';

const routes: Routes = [{ path: '', component: DemoPipesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoPipesRoutingModule { }
