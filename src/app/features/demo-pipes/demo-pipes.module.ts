import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoPipesRoutingModule } from './demo-pipes-routing.module';
import { DemoPipesComponent } from './demo-pipes.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DemoPipesComponent
  ],
  imports: [
    CommonModule,
    DemoPipesRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class DemoPipesModule { }
