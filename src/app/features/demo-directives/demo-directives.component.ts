import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fm-demo-directives',
  template: `
      <button fmUrl="http://www.google.com">google</button>
      <div fmIfSignin fmUrl="http://www.google.com">sono loggato</div>
      Hello <span [fmBg]="typeValue" [fmPad]="paddingValue">Famas</span>
      Hello <span [fmBg]="typeValue">Famas</span>
      Hello <span [fmBg]="typeValue">Famas</span>
      
      <button (click)="typeValue = 'warning'">warning</button>
      <button (click)="typeValue = 'danger'">danger</button>
      <button (click)="paddingValue = paddingValue + 1">inc pad</button>
      s
  `,
})
export class DemoDirectivesComponent implements OnInit {
  typeValue: 'success' | 'warning' | 'danger' = 'success'
  paddingValue: number = 3;
  constructor() { }

  ngOnInit(): void {
  }

}
