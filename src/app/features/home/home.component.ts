import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fm-home',
  template: `
    <fm-card></fm-card>
    <fm-hero></fm-hero>
    <fm-news></fm-news>
    <input type="text" [ngModel]>
    
    <fm-features></fm-features>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
