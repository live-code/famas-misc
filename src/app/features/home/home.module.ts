import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { FeaturesComponent } from './components/features.component';
import { HeroComponent } from './components/hero.component';
import { NewsComponent } from './components/news.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HomeComponent,
    FeaturesComponent,
    HeroComponent,
    NewsComponent,
  ],
  imports: [
    SharedModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: HomeComponent}
    ])
  ]
})
export class HomeModule {

}
