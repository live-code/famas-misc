import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'fm-catalog',
  template: `
    <button>shop</button>
    <button>offers</button>
    <button>lastminute</button>
  
    <hr>
    empty. Missing componens for router
    <router-outlet></router-outlet>
  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {

  constructor(private http: HttpClient) {
    http.get('http://localhost:3000/catalog')
      .subscribe(
        res => console.log('success'),
        err => console.log('failed')
      )
  }

  ngOnInit(): void {
  }

}
