import { Component } from '@angular/core';
import { AuthService } from './core/auth.service';

@Component({
  selector: 'fm-root',
  template: ` 
    <button routerLink="home">home</button>
    <button routerLink="demo-directives" fmIfSignin>directives</button>
    <button routerLink="demo-pipes" *fmIfLogged>pipes</button>
    <button routerLink="demo-reative-forms" *fmIfRoleIs="['moderator', 'admin']">forms</button>
    <button routerLink="login"  *ngIf="!(authService.isLogged$() | async)">login</button>
    <button routerLink="catalog" *ngIf="authService.isRoleValid$(['moderator', 'admin']) | async">catalog</button>
    <button (click)="authService.logout()" *ngIf="authService.isLogged$() | async">logout</button>

    <hr>
    <router-outlet></router-outlet>
    
  `,
  styles: []
})
export class AppComponent {
  constructor(public authService: AuthService) {

  }
}
