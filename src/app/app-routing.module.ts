import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth.guard';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)},
      { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule) },
      {
        path: 'catalog',
        loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule),
        canActivate: [AuthGuard]
      },
      { path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: 'demo-directives', loadChildren: () => import('./features/demo-directives/demo-directives.module').then(m => m.DemoDirectivesModule) },
      { path: 'demo-pipes', loadChildren: () => import('./features/demo-pipes/demo-pipes.module').then(m => m.DemoPipesModule) },
      { path: 'demo-reative-forms', loadChildren: () => import('./features/demo-reactiveforms/demo-reactiveforms.module').then(m => m.DemoReactiveformsModule) },
    ]),
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {

}
