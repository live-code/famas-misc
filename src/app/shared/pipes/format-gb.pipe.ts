import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatGb',
})
export class FormatGbPipe implements PipeTransform {

  transform(gb: number, format: 'kb' | 'mb' = 'mb', round: boolean = false): number {
    console.log('formatGb', gb, format, round)
    switch (format) {
      case 'kb': return gb * 1000000;
      case 'mb': return gb * 1000;
    }

  }

}
