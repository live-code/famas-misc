import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gmap'
})
export class GmapPipe implements PipeTransform {

  transform(city: string, zoom: number = 5): unknown {
    return `https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=${city}&zoom=${zoom}&size=600,400`;
  }

}
