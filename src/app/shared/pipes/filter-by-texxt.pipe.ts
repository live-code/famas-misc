import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../model/user';

@Pipe({
  name: 'filterByTexxt'
})
export class FilterByTexxtPipe implements PipeTransform {

  transform(users: User[], text: string): User[] {
    return users.filter(item => {
      return item.name.toLowerCase().includes(text.toLowerCase())
    });
  }

}
