import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../model/user';

@Pipe({
  name: 'filterByAge'
})
export class FilterByAgePipe implements PipeTransform {

  transform(users: User[], minAge: number): User[] {
    return users.filter(u => {
      return u.age >= minAge;
    });
  }

}
