import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../model/user';

@Pipe({
  name: 'sortBy'
})
export class SortByPipe implements PipeTransform {

  transform(users: User[], sortBy: 'ASC' | 'DESC'): User[] {
    const result = users.sort((a, b) => a.name.localeCompare(b.name));
    return sortBy === 'ASC' ? result : result.reverse();
  }

}
