import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'fm-card',
  template: `
    <p>
      card works!
    </p>
  `,
  styles: [
  ]
})
export class CardComponent implements OnInit {
  @Input() title!: string;
  @Output() iconClick = new EventEmitter()
  constructor() { }

  ngOnInit(): void {
  }

}
