import { Directive, ElementRef, HostBinding, Input, Renderer2, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[fmPad]'
})
export class PadDirective {
  @Input() set fmPad(val: number) {
    this.render.setStyle(this.el.nativeElement, 'padding', val + 'px')
  }

  constructor(private el: ElementRef, private render: Renderer2) {
  }

  /*ngOnChanges(changes: SimpleChanges) {
    if (changes.fmPad) {
      this.render.setStyle(this.el.nativeElement, 'padding', changes.fmPad.currentValue + 'px')
    }
  }*/
}
