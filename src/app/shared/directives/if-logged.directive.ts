import { Directive, ElementRef, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Directive({
  selector: '[fmIfLogged]'
})
export class IfLoggedDirective {
  destroy$ = new Subject();

  constructor(
    private tpl: TemplateRef<any>,
    private view: ViewContainerRef,
    private authService: AuthService
  ) {
    this.authService.isLogged$()
      .pipe(
        takeUntil(this.destroy$),
        distinctUntilChanged()
      )
      .subscribe(isLogged => {
        if (isLogged) {
          view.createEmbeddedView(tpl)
        } else {
          view.clear()
        }
      })
  }


  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
