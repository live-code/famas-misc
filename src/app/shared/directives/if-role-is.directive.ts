import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService, Role } from '../../core/auth.service';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Directive({
  selector: '[fmIfRoleIs]'
})
export class IfRoleIsDirective {
  @Input() fmIfRoleIs!: Role[];
  destroy$ = new Subject();

  constructor(
    private tpl: TemplateRef<any>,
    private view: ViewContainerRef,
    private authService: AuthService
  ) {

  }

  ngOnInit() {
    this.authService.isRoleValid$(this.fmIfRoleIs)
      .pipe(
        takeUntil(this.destroy$),
        distinctUntilChanged()
      )
      .subscribe(isValidRole => {
        if (isValidRole) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear()
        }
      })
  }


  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
