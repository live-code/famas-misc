import { Directive, ElementRef, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[fmUrl]'
})
export class UrlDirective {
  @Input() fmUrl!: string;

  @HostBinding('style.cursor') cursor = 'pointer'
  @HostBinding('style.color') color = 'blue'

  @HostListener('click')
  openUrl() {
    window.open(this.fmUrl)
  }

}
