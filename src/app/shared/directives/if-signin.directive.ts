import { Directive, ElementRef, HostBinding, OnDestroy, Renderer2 } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[fmIfSignin]'
})
export class IfSigninDirective implements OnDestroy {
  destroy$ = new Subject();

  constructor(
    private authService: AuthService,
    private el: ElementRef,
    private render: Renderer2
  ) {
    this.authService.isLogged$()
      .pipe(
        takeUntil(this.destroy$),
        distinctUntilChanged()
      )
      .subscribe(val => {
        this.render.setStyle(
          this.el.nativeElement,
          'display',
          val ? null : 'none'
        )
      })
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


  /*sub: Subscription;

  constructor(
    private authService: AuthService,
    private el: ElementRef,
    private render: Renderer2
  ) {
    this.sub = this.authService.isLogged$()
      .subscribe(val => {
        console.log('qui')
        this.render.setStyle(
          this.el.nativeElement,
          'display',
          val ? null : 'none'
        )
      })
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }*/

}
