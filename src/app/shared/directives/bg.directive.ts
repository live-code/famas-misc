import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[fmBg]'
})
export class BgDirective {
  @Input() fmBg: 'success' | 'warning' | 'danger' = 'warning';
  @HostBinding() get class() {
    return `highlight-${this.fmBg}`
  }
  @HostBinding('style.text-decoration') underline = 'underline';
}
