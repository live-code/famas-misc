import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { CardModule } from './components/card.module';
import { BgDirective } from './directives/bg.directive';
import { PadDirective } from './directives/pad.directive';
import { IfSigninDirective } from './directives/if-signin.directive';
import { UrlDirective } from './directives/url.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { IfRoleIsDirective } from './directives/if-role-is.directive';
import { FormatGbPipe } from './pipes/format-gb.pipe';
import { GmapPipe } from './pipes/gmap.pipe';
import { FilterByAgePipe } from './pipes/filter-by-age.pipe';
import { FilterByGenderPipe } from './pipes/filter-by-gender.pipe';
import { FilterByTexxtPipe } from './pipes/filter-by-texxt.pipe';
import { SortByPipe } from './pipes/sort-by.pipe';



@NgModule({
  declarations: [
    BgDirective,
    PadDirective,
    IfSigninDirective,
    UrlDirective,
    IfLoggedDirective,
    IfRoleIsDirective,
    FormatGbPipe,
    GmapPipe,
    FilterByAgePipe,
    FilterByGenderPipe,
    FilterByTexxtPipe,
    SortByPipe
  ],
  imports: [
    CommonModule,
    CardModule
  ],
  exports: [
    CardModule,
    BgDirective,
    PadDirective,
    IfSigninDirective,
    UrlDirective,
    IfLoggedDirective,
    IfRoleIsDirective,
    FormatGbPipe,
    GmapPipe,
    FilterByAgePipe,
    FilterByGenderPipe,
    FilterByTexxtPipe,
    SortByPipe
  ]
})
export class SharedModule { }
