export interface Auth {
  displayName: string;
  token: string;
  role: 'admin' | 'moderator' | 'guest'
}
